# xvid

a very simple [osc](http://opensoundcontrol.org/) triggered video image display.

the osc command set is:

~~~~
/tr         node:int id:int value:float
~~~~

default port is: 57151

requests notifications from scsynth at port: 57110
