# gl-osc

[open gl](http://www.opengl.org/) renderer with [osc](http://opensoundcontrol.org/) control

the osc command set is:

~~~~
/ln1        dur:time x0:float y0:float x1:float y1:float
            draw line (x0,y0)->(x1,y1) for dur seconds
~~~~

default port is: 57149
