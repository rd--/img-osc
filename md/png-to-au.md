# png-to-au

Convert one channel of an RGB [PNG](http://www.libpng.org/) file to a
32 bit floating point NeXT/AU sound file.  The conversion is in row
order, ie. there are `height` number of channels and `width` number of
frames.

~~~~
$ png-to-au -h
png-to-au {r|g|b} png-file au-file
$
~~~~
