img-osc
-------

image display engine with [osc][osc] control

the osc command set is:

~~~~
/load       id:int name:string
            load named file to indicated buffer (async)
/save       name:string
            write current surface contents to named file (async)
/cpy        id:int src-x:int src-y:int w:int h:int dst-x:int dst-y:int
            copy indicated area of buffer to video display buffer
/fill       x:int y:int w:int h:int red:int green:int blue:int alpha:int
            fill indicated area with indicated color
/cpy_all    id:int
            copy indicated buffer to video display buffer
/blit       blit video display buffer
/resize     w:int h:int
            resize video display
/resize_for id:int
            resize video display to size of indicated buffer
~~~~

default port is: 57147

requires:

- [hosc][hosc]
- [SDL][hs-sdl]
- [SDL-image][hs-sdl-image]

debian:

- SLD 1.2 => sudo apt-get install libsdl1.2-dev libsdl-image1.2-dev
- SDL 2 => sudo apt-get install libsdl2-dev libsdl2-image-dev

[osc]: http://opensoundcontrol.org/
[hosc]: http://rd.slavepianos.org/?t=hosc
[hs-sdl]: http://hackage.haskell.org/package/SDL
[hs-sdl-image]: http://hackage.haskell.org/package/SDL-image
