# cjset

real-time/interactive [cairo][cairo] julia set renderer that can
generate [osc][osc] control streams for the
[supercollider][sc3] synthesiser.

~~~~
$ cjset -h
cjset [-c int] [-n int] [-o string] [-p real] [-s int] [-t real] [-u real] [-v]
  -c control index = -1 (send /c_setn messages to scsynth)
  -n iterations = 5000
  -o synthdef name = nil (send /s_new messages to scsynth)
  -p pixels = 1
  -s size = 900
  -t frames-per-second = 24
  -u user-space = 2
  -v print messages to stdout = false
$
~~~~

[osc]: http://opensoundcontrol.org/
[cairo]: http://cairographics.org/
[sc3]: http://audiosynth.com/