-- xvid ~/sw/img-osc/png/*.png

import Control.Monad {- base -}
import Control.Monad.Random {- MonadRandom -}

import Sound.Osc {- hosc -}

rrand :: MonadRandom m => Double -> Double -> m Double
rrand l r = getRandomR (l, r)

trigger :: (MonadRandom m, Transport m) => m ()
trigger = do
  n <- rrand 0.15 1
  sendMessage (Message "/tr" [int32 0, int32 0, float n])
  t <- rrand 0.025 0.35
  pauseThread t

withXvid :: Connection OscSocket a -> IO a
withXvid = withTransport (openOscSocket (Udp, "127.0.0.1", 57151))

main :: IO ()
main = withXvid (replicateM_ 512 trigger)
