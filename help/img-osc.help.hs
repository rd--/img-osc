-- img-osc variant of xvid.help.hs

import Control.Monad {- base -}
import Control.Monad.Random {- MonadRandom -}

import Sound.Osc {- hosc -}
import Sound.Sc3 {- hosc -}

with_img_osc :: Connection OscSocket a -> IO a
with_img_osc = withTransport (openOscSocket (Udp, "127.0.0.1", 57147))

prj_file :: FilePath -> FilePath
prj_file nm = "/home/rohan/sw/img-osc/" ++ nm

rand_f64 :: MonadRandom m => Double -> Double -> m Double
rand_f64 l r = getRandomR (l, r)

rand_i32 :: MonadRandom m => Int -> Int -> m Int
rand_i32 l r = getRandomR (l, r)

trigger :: (MonadRandom m, Transport m) => m ()
trigger = do
  sendMessage (message "/fill_all" (map int32 [0, 0, 0, 255]))
  a <- rand_i32 38 255
  sendMessage (message "/set_alpha" [int32 0, int32 a])
  sendMessage (message "/cpy_all" [int32 0])
  sendMessage (message "/blit" [])
  t <- rand_f64 0.025 0.35
  pauseThread t

setup :: Transport m => m ()
setup = do
  _ <- async (message "/load" [int32 0, string (prj_file "png/flower.png")])
  _ <- async (message "/resize_for" [int32 0])
  return ()

main :: IO ()
main = with_img_osc (setup >> replicateM_ 512 trigger)
