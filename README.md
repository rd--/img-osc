# img-osc

images & open sound control

## cli

[cjset](?t=img-osc&e=md/cjset.md),
[gl-osc](?t=img-osc&e=md/gl-osc.md),
[img-osc](?t=img-osc&e=md/img-osc.md),
[png-to-au](?t=img-osc&e=md/png-to-au.md),
[xvid](?t=img-osc&e=md/xvid.md)

tested-with:
[ghc](http://www.haskell.org/ghc/)-9.10.1
[gcc](http://gcc.gnu.org/)-10.2.1
[clang](https://clang.llvm.org/)-11.0.1

debian: libsdl1.2-dev libsdl-image1.2-dev

© [rohan drape](http://rohandrape.net/),
  1998-2024,
  [gpl](http://gnu.org/copyleft/)
