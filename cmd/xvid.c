#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

/* r-common/c */
#include "r-common/c/byte-order.h"
#include "r-common/c/float.h"
#include "r-common/c/int.h"
#include "r-common/c/memory.h"
#include "r-common/c/network.h"
#include "r-common/c/osc.h"

#include "r-common/c/img-png.c"
#include "r-common/c/x11.c"
#include "r-common/c/ximg.c"

struct xvid {
	f32 ctl[512];
	i32 ctln;
	int fd;
};

void request_notification(int fd)
{
	struct sockaddr_in addr;
	init_sockaddr_in(&addr, "localhost", 57110);
	const int osc_pkt_extent = 64;
	uint8_t osc_pkt[osc_pkt_extent];
	int osc_pkt_sz = osc_build_message(osc_pkt, osc_pkt_extent, "/notify", ",i", 1);
	sendto_exactly(fd, osc_pkt, osc_pkt_sz, addr);
}

bool recv_trigger(int fd, i32 *node, i32 *id, f32 *value)
{
	const i32 packet_extent = 64;
	u8 packet[packet_extent];
	i32 packet_sz = xrecv(fd, packet, packet_extent, 0);
	osc_data_t o[3];
	if (osc_parse_message("/tr", ",iif", packet, packet_sz, o)) {
		*node = o[0].i;
		*id = o[1].i;
		*value = o[2].f;
		return true;
	} else {
		fprintf(stderr, "dropped packet=%s\n", packet);
	}
	return false;
}

int main(int argc, char *argv[])
{
	if (argc < 2) {
		fprintf(stderr, "xvid png-image-file...\n");
		return EXIT_FAILURE;
	}

	struct xvid v;
	v.ctln = 512;
	v.fd = socket_udp(0);
	int port_n = 57151;
	bind_inet(v.fd, NULL, port_n);
	if (true) {
		request_notification(v.fd);
	}

	i32 w = 0, h = 0;
	i32 n = argc - 1;
	printf("loading images...\n");
	u8 **frm = load_png_rgb8_seq(argv + 1, n, &w, &h);
	if (frm == NULL) {
		fprintf(stderr, "load_png_rgb8_seq failed\n");
		return EXIT_FAILURE;
	}
	printf("\rn=%d, w=%d, h=%d\n", n, w, h);

	u8 *pst = xmalloc(w * h * 3);
	Ximg_t *ximg = ximg_open(w, h, "xvid");
	printf("entering main loop\n");
	while (true) {
		i32 node, id = 0, j;
		f32 value = 0, scalar;
		recv_trigger(v.fd, &node, &id, &value);
		/*fprintf(stderr,"value=%f\n", value); */
		/* id=0 -> value = scalar and 0-1 frame index */
		/* id=1 -> value = frame index */
		if (id == 0) {
			scalar = value;
			if (value < 0.0) {
				scalar = 0.0;
			}
			j = (i32)floorf((f32)n * value);
			j = j % n;
		} else if (id == 1) {
			scalar = 1.0;
			j = (i32)floorf(value);
			j = j % n;
		} else {
			j = 0;
			scalar = 1.0;
		}
		for (i32 i = 0; i < w * h * 3; i++) {
			pst[i] = frm[j][i] * scalar;
		}
		ximg_blit(ximg, pst);
	}

	ximg_close(ximg);
	return EXIT_SUCCESS;
}
