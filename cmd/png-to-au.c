#include <stdio.h>
#include <stdlib.h>

#include "r-common/c/failure.h"
#include "r-common/c/int.h"
#include "r-common/c/memory.h"
#include "r-common/c/sf-au.h"

#include "r-common/c/img-png.c"

int main(int argc, char **argv)
{
	if (argc != 4) {
		die("png-to-au {r|g|b} png-file au-file\n");
	}
	i32 ch = 0;
	switch (argv[1][0]) {
	case 'r':
		ch = 0;
		break;
	case 'g':
		ch = 1;
		break;
	case 'b':
		ch = 2;
		break;
	default:
		die("'%s' is not 'r', 'g' or 'b'\n", argv[1]);
	}
	char *img_nm = argv[2];
	char *au_nm = argv[3];
	i32 w = 0, h = 0;
	u8 *frm;
	if (!load_png_rgb8(img_nm, &w, &h, &frm)) {
		die("load_png_rgb8 failed\n");
	}
	float *au = xmalloc(w * h * sizeof(float));
	for (i32 i = 0; i < w; i++) {
		for (i32 j = 0; j < h; j++) {
			au[i * h + j] = (float)frm[(j * w + i) * 3 + ch] / 255.0;
			if (i == 0) {
				printf("%d -> %f\n", (int)frm[i * 3], au[i]);
			}
		}
	}
	free(frm);
	write_au_f32(au_nm, h, w, 1, au);
	return 0;
}
