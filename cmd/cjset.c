#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <X11/Xlib.h>
#include <cairo-xlib.h>
#include <cairo.h>

#include <lo/lo.h>

#include "r-common/c/time-current.c"
#include "r-common/c/time-pause.c"
#include "r-common/c/x11.c"

#define PI 3.14159265358979323846

typedef struct {
	int x;
	int y;
} pt_t;

typedef struct {
	double x;
	double y;
} point_t;

typedef struct {
	double u_sp;
	double sz;
	int n_iter;
	double px_m;
	char *sc3_node;
	int sc3_ctl;
	double fps;
	bool verbose;
} cjset_t;

unsigned int x_mouse(Display *d, Window w, int *x, int *y)
{
	Window root, child;
	int rootx, rooty;
	unsigned int mask;
	XQueryPointer(d, w, &root, &child, &rootx, &rooty, x, y, &mask);
	return mask;
}

void lo_status(lo_address fd, int err)
{
	if (err == -1) {
		int n = lo_address_errno(fd);
		const char *s = lo_address_errstr(fd);
		fprintf(stderr, "error %d: %s\n", n, s);
	}
}

void send_s_new(lo_address fd, char *nm, point_t p)
{
	float x = (float)p.x;
	float y = (float)p.y;
	int err = lo_send(fd, "/s_new", "siiisfsf", nm, -1, 1, 1, "x", x, "y", y);
	lo_status(fd, err);
}

void send_c_setn(lo_address fd, int k, point_t p)
{
	float x = (float)p.x;
	float y = (float)p.y;
	int err = lo_send(fd, "/c_setn", "iiff", k, 2, x, y);
	lo_status(fd, err);
}

void jset_iim(cjset_t *j, cairo_surface_t *cs, lo_address fd, pt_t dev_c)
{
	cairo_t *cr = cairo_create(cs);

	/* white background */
	cairo_rectangle(cr, 0.0, 0.0, j->sz, j->sz);
	cairo_set_source_rgb(cr, 1.0, 1.0, 1.0);
	cairo_fill(cr);

	/* black foreground */
	cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);

	/* user space is (-n,n) */
	cairo_scale(cr, j->sz / (2 * j->u_sp), j->sz / (2 * j->u_sp));
	cairo_translate(cr, j->u_sp, j->u_sp);

	/* one pixel in user space */
	double px_x = 1.0 * j->px_m;
	double px_y = 1.0 * j->px_m;
	cairo_device_to_user_distance(cr, &px_x, &px_y);

	/* translate c (mouse coordinate) to user space */
	point_t c;
	c.x = (double)dev_c.x;
	c.y = (double)dev_c.y;
	cairo_device_to_user(cr, &c.x, &c.y);

	point_t a = { 0.0, 0.0 }, w;
	for (int i = 0; i < j->n_iter; i++) {
		double theta = 0.0, r = 0.0;
		w.x = a.x - c.x;
		w.y = a.y - c.y;
		if (w.x > 0.0) {
			theta = atan(w.y / w.x);
		} else if (w.x < 0.0) {
			theta = PI + atan(w.y / w.x);
		} else {
			theta = PI / 2.0;
		}
		theta /= 2.0;
		r = sqrt(w.x * w.x + w.y * w.y);
		if (rand() % 2 == 0) {
			r = sqrt(r);
		} else {
			r = -1.0 * sqrt(r);
		}
		a.x = r * cos(theta);
		a.y = r * sin(theta);
		if (fabs(a.x) < j->u_sp && fabs(a.y) < j->u_sp) {
			cairo_rectangle(cr, a.x, a.y, px_x, px_y);
			cairo_fill(cr);
			if (j->sc3_node) {
				send_s_new(fd, j->sc3_node, a);
			}
			if (j->sc3_ctl >= 0) {
				send_c_setn(fd, (i * 2) + j->sc3_ctl, a);
			}
			if (j->verbose) {
				printf("iter: %d node: %s ctl: %d x: %f y: %f\n",
					i, j->sc3_node, (i * 2) + j->sc3_ctl, a.x, a.y);
			}
		}
	}
	cairo_show_page(cr);
	cairo_destroy(cr);
}

void cjset(cjset_t *j)
{
	Display *d = x11_XOpenDisplay_err(NULL);
	x11_fail_on_errors();
	int s = DefaultScreen(d);
	Window r = RootWindow(d, s);
	unsigned long b = BlackPixel(d, s);
	Window w = XCreateSimpleWindow(d, r, 1, 1, j->sz, j->sz, 0, b, b);
	XStoreName(d, w, "cjset");
	XMapWindow(d, w);

	Visual *v = DefaultVisual(d, 0);
	cairo_surface_t *cs = cairo_xlib_surface_create(d, w, v, (double)j->sz, (double)j->sz);

	char scsynth_port[] = "57110";
	lo_address fd = lo_address_new(NULL, scsynth_port);

	double t = current_time_as_utc_real();
	while (1) {
		pt_t m;
		x_mouse(d, w, &m.x, &m.y);
		jset_iim(j, cs, fd, m);
		t += 1 / j->fps;
		pause_until(t);
	}

	cairo_surface_destroy(cs);
	lo_address_free(fd);
	XCloseDisplay(d);
}

void usage(int err)
{
	printf("cjset [-c int] [-n int] [-o string] [-p real] [-s int] [-t real] [-u real] [-v]\n");
	printf("  -c control index = -1 (send /c_setn messages to scsynth)\n");
	printf("  -n iterations = 5000\n");
	printf("  -o synthdef name = nil (send /s_new messages to scsynth)\n");
	printf("  -p pixels = 1\n");
	printf("  -s size = 900\n");
	printf("  -t frames-per-second = 24\n");
	printf("  -u user-space = 2\n");
	printf("  -v print messages to stdout = false\n");
	exit(err);
}

int main(int argc, char **argv)
{
	int c;
	cjset_t j = { 2.0, 900, 5000, 1.0, NULL, -1, 24, false };
	while ((c = getopt(argc, argv, "c:hn:o:p:s:t:u:v")) != -1) {
		switch (c) {
		case 'c':
			j.sc3_ctl = (int)strtol(optarg, NULL, 0);
			break;
		case 'h':
			usage(0);
		case 'n':
			j.n_iter = (int)strtol(optarg, NULL, 0);
			break;
		case 'o':
			j.sc3_node = strdup(optarg);
			break;
		case 'p':
			j.px_m = atof(optarg);
			break;
		case 's':
			j.sz = (int)strtol(optarg, NULL, 0);
			break;
		case 't':
			j.fps = atof(optarg);
			break;
		case 'u':
			j.u_sp = atof(optarg);
			break;
		case 'v':
			j.verbose = true;
			break;
		default:
			usage(1);
		}
	}
	cjset(&j);
}
