all:
	echo "img-osc"

mk-cmd:
	(cd cmd ; make all install)

clean:
	rm -Rf dist dist-newstyle *~
	(cd cmd ; make clean)

dep-debian:
	sudo apt-get install libsdl1.2-dev libsdl-image1.2-dev

dep-cabal:
	cabal v1-install SDL SDL-image GLUT --allow-newer

push-all:
	r.gitlab-push.sh img-osc

indent:
	fourmolu -i Sound cmd help

doctest:
	doctest -Wno-x-partial -Wno-incomplete-uni-patterns Sound
