module Sound.Osc.Image.Sdl where

import Control.Monad {- base -}
import qualified Data.ByteString.Char8 as C {- bytestring -}
import qualified Data.Map as M {- containers -}
import Data.Maybe {- base -}
import Data.Word {- base -}

import qualified Graphics.UI.SDL as Sdl {- SDL -}
import qualified Graphics.UI.SDL.Image as Sdl.Image {- SDL-image -}

import Sound.Osc.Core {- hosc -}
import qualified Sound.Osc.Transport.Fd.Udp as Osc.Fd.Udp {- hosc -}

data Img = Img
  { im :: M.Map Int Sdl.Surface
  , fd :: Osc.Fd.Udp.Udp
  }

mk_img :: IO Img
mk_img = do
  u <- Osc.Fd.Udp.udpServer "127.0.0.1" 57147
  _ <- Sdl.setVideoMode 100 100 24 [Sdl.SWSurface]
  Sdl.setCaption "img.osc" ""
  Sdl.enableUnicode True
  return (Img M.empty u)

img_load :: Img -> Int -> String -> IO Img
img_load i n f = do
  s <- Sdl.Image.load f
  let m = M.insert n s (im i)
  return (i {im = m})

img_save :: String -> IO Bool
img_save f = do
  s <- Sdl.getVideoSurface
  Sdl.saveBMP s f

img_blit :: Img -> IO Img
img_blit i = do
  s <- Sdl.getVideoSurface
  Sdl.flip s
  return i

img_cpy_all :: Img -> Int -> IO Img
img_cpy_all i n = do
  s <- Sdl.getVideoSurface
  _ <- Sdl.blitSurface (im i M.! n) Nothing s Nothing
  return i

img_cpy :: Img -> Int -> (Int, Int) -> (Int, Int) -> (Int, Int) -> IO Img
img_cpy i n (xs, ys) (w, h) (xd, yd) = do
  s <- Sdl.getVideoSurface
  let r0 = Sdl.Rect xs ys w h
      r1 = Sdl.Rect xd yd w h
  _ <- Sdl.blitSurface (im i M.! n) (Just r0) s (Just r1)
  return i

img_fill :: (Int, Int) -> (Int, Int) -> (Int, Int, Int, Int) -> IO Bool
img_fill (x, y) (w, h) (r, g, b, a) = do
  s <- Sdl.getVideoSurface
  let pf = Sdl.surfaceGetPixelFormat s
  p <- Sdl.mapRGBA pf (u8 r) (u8 g) (u8 b) (u8 a)
  Sdl.fillRect s (Just (Sdl.Rect x y w h)) p

img_fill_all :: (Int, Int, Int, Int) -> IO Bool
img_fill_all c = do
  s <- Sdl.getVideoSurface
  let w = Sdl.surfaceGetWidth s
      h = Sdl.surfaceGetHeight s
  img_fill (0, 0) (w, h) c

img_resize :: Img -> Int -> Int -> IO Img
img_resize i w h = do
  _ <- Sdl.setVideoMode w h 24 [Sdl.SWSurface]
  return i

img_resize_for :: Img -> Int -> IO Img
img_resize_for i n = do
  let w = Sdl.surfaceGetWidth (im i M.! n)
      h = Sdl.surfaceGetHeight (im i M.! n)
  img_resize i w h

u8 :: Int -> Word8
u8 = fromIntegral

img_set_alpha :: Img -> Int -> Int -> IO Img
img_set_alpha i n a = do
  let s = im i M.! n
  r <- Sdl.setAlpha s [Sdl.SrcAlpha] (u8 a)
  when (not r) (print "img_set_alpha: error")
  return i

sync :: IO Img -> IO (Bool, Img)
sync = liftM ((,) False)

async :: IO Img -> IO (Bool, Img)
async = liftM ((,) True)

as_int :: Datum -> Int
as_int = fromMaybe (error "as_int") . datum_integral

img_ctl_p :: Img -> PacketOf Message -> IO (Bool, Img)
img_ctl_p i = at_packet (img_ctl_m i) (img_ctl_b i)

img_ctl_m :: Img -> Message -> IO (Bool, Img)
img_ctl_m i m = do
  let fi = fromIntegral
  case m of
    Message "/load" [Int32 n, AsciiString fn] ->
      async (img_load i (fi n) (C.unpack fn))
    Message "/save" [AsciiString fn] ->
      async (img_save (C.unpack fn) >> return i)
    Message "/cpy" [n, xs, ys, w, h, xd, yd] ->
      sync
        ( img_cpy
            i
            (as_int n)
            (as_int xs, as_int ys)
            (as_int w, as_int h)
            (as_int xd, as_int yd)
        )
    Message "/fill" [x, y, w, h, r, g, b, a] ->
      sync
        ( img_fill
            (as_int x, as_int y)
            (as_int w, as_int h)
            (as_int r, as_int g, as_int b, as_int a)
            >> return i
        )
    Message "/fill_all" [r, g, b, a] ->
      sync
        ( img_fill_all
            (as_int r, as_int g, as_int b, as_int a)
            >> return i
        )
    Message "/cpy_all" [n] -> sync (img_cpy_all i (as_int n))
    Message "/blit" [] -> sync (img_blit i)
    Message "/resize" [Int32 w, Int32 h] ->
      async (img_resize i (fi w) (fi h))
    Message "/resize_for" [Int32 n] -> async (img_resize_for i (fi n))
    Message "/set_alpha" [Int32 n, Int32 a] -> sync (img_set_alpha i (fi n) (fi a))
    _ -> putStrLn ("dropped packet: " ++ show m) >> async (return i)

img_ctl_b :: Img -> BundleOf Message -> IO (Bool, Img)
img_ctl_b i (Bundle _ m) = liftM last (mapM (img_ctl_m i) m)

work :: Img -> IO ()
work img = do
  (pkt, adr) <- Osc.Fd.Udp.recvFrom (fd img)
  (s, img') <- img_ctl_p img pkt
  when s (Osc.Fd.Udp.sendTo (fd img) (p_message "/done" []) adr)
  work img'

img_sdl_main :: IO ()
img_sdl_main = Sdl.withInit [Sdl.InitVideo] (mk_img >>= work)
