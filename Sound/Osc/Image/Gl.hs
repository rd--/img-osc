module Sound.Osc.Image.Gl where

import Control.Concurrent {- base -}
import Control.Monad {- base -}
import qualified Data.Foldable as Foldable {- base -}
import qualified Data.IORef as IoRef {- base -}
import qualified Data.Sequence as Sequence {- containers -}
import System.Exit {- base -}

import Graphics.UI.GLUT {- glut -}

import qualified Sound.Osc.Fd as Osc {- hosc -}
import qualified Sound.Osc.Text as Osc.Text {- hosc -}
import qualified Sound.Osc.Transport.Fd.Udp as Osc.Udp {- hosc -}

-- * Instructions & State

type Instruction = (Osc.Time, Osc.Message)
type Instructions = Sequence.Seq Instruction
data State = State {instructions :: IoRef.IORef Instructions}

make_state :: IO State
make_state = do
  i <- IoRef.newIORef Sequence.empty
  return (State i)

add_instruction :: Osc.Time -> Osc.Message -> Instructions -> Instructions
add_instruction t m i =
  case m of
    Osc.Message _ (Osc.TimeStamp dt : _) ->
      let t' = t + dt
      in i Sequence.|> (t', m)
    _ -> i

message_expired :: Osc.Time -> Instruction -> Bool
message_expired t (t', _) = t' >= t

decay_instructions :: Osc.Time -> Instructions -> Instructions
decay_instructions t = Sequence.filter (message_expired t)

list_instructions :: State -> IO ()
list_instructions st = do
  t <- Osc.time
  i <- get (instructions st)
  putStrLn ("current time: " ++ show t)
  putStrLn "instructions:"
  Foldable.traverse_ (putStrLn . Osc.Text.showMessage Nothing . snd) i
  putStrLn ("# instructions: " ++ show (Sequence.length i))

-- * Drawing

type F32 = GLfloat
type V3 = Vertex3 F32

p2_to_v3 :: Float -> Float -> V3
p2_to_v3 x y = Vertex3 (realToFrac x) (realToFrac y) 0

scale_f32 :: F32 -> F32 -> F32 -> IO ()
scale_f32 = scale

translate_f32 :: Vector3 GLfloat -> IO ()
translate_f32 = translate

draw_ln1 :: Float -> Float -> Float -> Float -> IO ()
draw_ln1 x0 y0 x1 y1 =
  renderPrimitive
    Lines
    ( vertex (p2_to_v3 x0 y0)
        >> vertex (p2_to_v3 x1 y1)
    )

draw_instruction :: Instruction -> IO ()
draw_instruction (_, m) =
  case m of
    Osc.Message "/ln1" [_, Osc.Float x0, Osc.Float y0, Osc.Float x1, Osc.Float y1] ->
      draw_ln1 x0 y0 x1 y1
    _ -> return ()

draw_state :: State -> IO ()
draw_state st = do
  t <- Osc.time
  i <- get (instructions st)
  clear [ColorBuffer]
  preservingMatrix
    ( translate_f32 (Vector3 (-2) (-2) (-2))
        >> scale_f32 4 4 4
        >> Foldable.traverse_ draw_instruction i
    )
  instructions st $= decay_instructions t i
  swapBuffers

-- * Incoming messages

listener :: Osc.Transport t => t -> IoRef.IORef Instructions -> IO ()
listener fd i = do
  m <- Osc.recvMessage fd
  t <- Osc.time
  case m of
    Nothing -> return ()
    Just m' -> IoRef.modifyIORef i (add_instruction t m')

worker :: IoRef.IORef Instructions -> IO ()
worker i = do
  fd <- Osc.Udp.udpServer "127.0.0.1" 57149
  forever (listener fd i)

-- * Shell

idle :: IO ()
idle = postRedisplay Nothing

userexit :: State -> IO t
userexit _ = exitWith ExitSuccess

keyboard :: State -> Key -> KeyState -> Modifiers -> Position -> IO ()
keyboard st k _ _ _ =
  case k of
    Char 'i' -> list_instructions st
    Char 'Q' -> userexit st
    _ -> return ()

init' :: IO ()
init' = do
  viewport $= (Position 0 0, Size 800 800)
  matrixMode $= Projection
  loadIdentity
  perspective 30 1 1 100
  matrixMode $= Modelview 0
  loadIdentity
  lookAt (Vertex3 0 0 10) (Vertex3 0 0 0) (Vector3 0 1 0)
  shadeModel $= Smooth
  clearColor $= Color4 0 0 0 0
  blend $= Enabled
  blendFunc $= (SrcAlpha, One)
  depthFunc $= Nothing
  lighting $= Disabled
  normalize $= Enabled

visible :: Visibility -> IO ()
visible v =
  case v of
    Visible -> idleCallback $= Just idle
    NotVisible -> idleCallback $= Nothing

img_gl_main :: IO ()
img_gl_main = do
  _ <- getArgsAndInitialize
  initialDisplayMode $= [RGBAMode, DoubleBuffered]
  initialWindowSize $= Size 800 800
  initialWindowPosition $= Position 0 0
  _ <- createWindow "GL"
  st <- make_state
  init'
  displayCallback $= draw_state st
  keyboardMouseCallback $= Just (keyboard st)
  visibilityCallback $= Just visible
  _ <- forkIO (worker (instructions st))
  mainLoop

{-
-- * Testing

gen_ln1 :: Osc.Time -> Float -> Float -> Float -> Float -> Osc.Message
gen_ln1 dt x0 y0 x1 y1 =    Osc.message "/ln1" [Osc.TimeStamp dt                     ,Osc.Float x0,Osc.Float y0,Osc.Float x1,Osc.Float y1]

fd <- Osc.openUDP "127.0.0.1" 57149
let f x0 y0 x1 y1 = Osc.sendMessage fd (gen_ln1 10 x0 y0 x1 y1)
f 0 0 1 0
f 0 0 0 1
f 0 0 1 1
f 0 1 1 1
f 1 1 1 0
f 0 1 1 0
-}
